#!/bin/bash

windowTitle='Steamdeck File Sharing'

main(){
	while true
	do
		option=`zenity --title "$windowTitle" --list --column 'Option' 'Change Password' 'Start SSHD' 'Connection Info' 'How To Connect' 'Exit'`
	
		case "$option" in
			"Change Password" ) changePW ;;
			"Start SSHD" ) startSSH ;;
			"Connection Info" ) connectInfo ;;
			"How To Connect" ) howConnect ;;
			"Exit" ) exit ;;
			* ) if [ $? -ne 0 ]; then exit; fi ;;
		esac
	done
}
changePW(){
	password=`zenity --title "$windowTitle" \
		--forms --text='Change User Account Password'\
	       	--add-password='Current Password:'\
	       	--add-password='New Password:'\
		--add-password='Confirm New Password:' --separator='!!&!!'`
	if [ $? -ne 0 ]
	then
		zenity --title "$windowTitle" --info --text='Password not changed'
	else
		echo $password | sed 's/!!&!!/\n/g' | passwd
		if [ $? -ne 0 ]
		then
			zenity --title "$windowTitle" --error --text='Unable to change password. Please ensure your old password is correct and that your new passwords match.'
		else
			zenity --title "$windowTitle" --info --text='Password changed successfully.'
		fi
	fi
#	echo $password | sed 's/!!&!!/\n/g'
}
startSSH(){
	zenity --title "$windowTitle" --question --text='Would you like to start the ssh server for remote shell access and file sharing?' --ok-label=Yes --cancel-label=No
	if [ $? -ne  0 ]
	then
		# User wants sshd stopped.
		systemctl stop sshd
		systemctl is-active sshd
		if [ $? -eq 3 ]
		then
			zenity --title "$windowTitle" --question --text 'sshd has been stopped. Would you like to disable sshd so it does not run on startup?' --ok-label="Yes, disable it." --cancel-label="No, I would like it enabled."
			if [ $? -ne 0 ]
			then
				zenity --title "$windowTitle" --info --text 'sshd has been enabled.'
			else
				zenity --title "$windowTitle" --info --text 'sshd has been disabled.'
			fi
		else
			zenity --title "$windowTitle" --error --text 'An error has occured and sshd might not have been stopped.'
		fi
	else
		# User wants sshd started.
		systemctl start sshd
		if [ $? -eq 0 ]
		then
			zenity --title "$windowTitle" --question --text 'sshd has been started. Would you like to enable sshd so it can run on startup?' --ok-label="Yes, enable it." --cancel-label="No, I would like it disabled."
			if [ $? -ne 0 ]
			then
				zenity --title "$windowTitle" --info --text 'sshd has been disabled.'
			else
				zenity --title "$windowTitle" --info --text 'sshd has been enabled.'
			fi
		else
			zenity --title "$windowTitle" --info --text 'An error has occured and sshd might not have been started.'
		fi
	fi
}
connectInfo(){
grep -q -e '^Port' /etc/ssh/sshd_config
if [ $? -ne 0 ]
then
	port=22
else
	port=`grep -e '^Port' /etc/ssh/sshd_config | tail -n 1 | cut -d ' ' -f 2`

fi

msg=`mktemp`

cat << EOF > $msg
Port to use: $port
IPv4 addresse(s):
`ip -o a | grep -v ': lo ' | grep 'inet ' | awk '{ print "    " $4 }' | cut -d '/' -f 1`
IPv6 addresse(s):
`ip -o a | grep -v ': lo ' | grep 'inet6 ' | awk '{ print "    " $4 }' | cut -d '/' -f 1`
Connection Protocols
    SSH
    SCP
    SFTP
EOF
zenity --title "$windowTitle" --text-info --filename="$msg"

rm $msg
}
howConnect(){
	xdg-open "https://gitlab.com/minecraftchest1/steamdeck-file-share-tool/-/wikis/How%20to%20Connect" & &>/dev/null
	zenity --title "$windowTitle" --info --text "Please visit https://gitlab.com/minecraftchest1/steamdeck-file-share-tool/-/wikis/How%20to%20Connect if you browser did not open already."
}

main
